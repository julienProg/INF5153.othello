/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.presentation;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.util.Duration;

/**
 *
 * @author Julien
 */
public class DessinateurInformationPartie {
           
           private HBox reference;
           public DessinateurInformationPartie(){
               
           }
    
           public void ajouterTextForXSeconde(int seconde, HBox container, String text){
               reference = container;
               container.setVisible(true);
                reference.toFront();
               
               Label titre = new Label(text);               
               titre.setStyle("-fx-background-color: #F5F5DC");
               titre.setFont(new Font("Arial", 50));
               
               container.getChildren().add(titre);
 
               cacherApresXSeconde(seconde);
           }
            
           private void cacherApresXSeconde(int seconde){
               Timeline timeline = new Timeline();
               timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(seconde),
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {
                        reference.setVisible(false);
                        reference.toBack();
                    }
                }));
                timeline.play();
           }
}
