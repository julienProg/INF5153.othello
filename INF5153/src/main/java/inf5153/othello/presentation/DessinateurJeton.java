/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.presentation;

/**
 *
 * @author Julien
 */

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.layout.Pane;

public class DessinateurJeton {
    
    public DessinateurJeton(){
    }
    
    public void dessinerUnJeton(Pane p, Color c){
        double w = p.getWidth();
        double h = p.getHeight();
        double x = p.getLayoutX();
        double y = p.getLayoutY();
        
        double rayon = 0.8*(w/2);
        
        
        Circle circle = new Circle((w/2), (h/2),rayon, c);
       // circle.setFill(gradient1);
        p.getChildren().add(circle);
 
    }
}
