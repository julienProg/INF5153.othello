/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.presentation;

import inf5153.othello.game.EtatCase;
import inf5153.othello.game.Plateau;
import inf5153.othello.game.TypeIA;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Julien
 */
public class Fenetre {
    private final InterfaceMainApp i;

    private final int width;
    private final int heigth;
    private final int widthMenu;
    private final int widthAffihcerInfo;

    private Label joueurBlanc;
    private Label joueurNoir;
    private Label nbJetonJB;
    private Label nbJetonJN;
    private Label stat;

    private Button btnDebutIAF;
    private Button btnDebutIAD;
    private Button btnCharger;
    private Button btnSauvegarder;
    private Button btnVisualiser;

    private DessinateurJeton dessinerJeton;
    private DessinateurInformationPartie dessinerText;

    private List<Pane> listeCase;

    public Fenetre(int w, int h, InterfaceMainApp i, int widthMenu, int withInfoPLateau) {
        width = w;
        heigth = h;
        this.i = i;
        this.widthMenu = widthMenu;
        widthAffihcerInfo = withInfoPLateau;
        listeCase = new ArrayList<>();
        dessinerJeton = new DessinateurJeton();
        dessinerText = new DessinateurInformationPartie();
    }

    public Parent afficherMenu() {

        VBox listeDeBouton = new VBox();
        listeDeBouton.setPrefWidth(widthMenu);

        btnDebutIAF = creerBouton("Débuter contre IA facile", listeDeBouton.getPrefWidth());
        btnDebutIAF.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                i.commencerUnePartie(TypeIA.hasard);
            }
        });

        btnDebutIAD = creerBouton("Débuter contre IA difficile", listeDeBouton.getPrefWidth());
        btnDebutIAD.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                i.commencerUnePartie(TypeIA.grosCoup);
            }
        });

        btnCharger = creerBouton("Charger", listeDeBouton.getPrefWidth());
        btnCharger.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                i.charger();
            }
        });

        btnSauvegarder = creerBouton("Sauvegarder", listeDeBouton.getPrefWidth());
        btnSauvegarder.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                i.sauvegarder();
            }
        });

        btnVisualiser = creerBouton("Visualiser la Partie", listeDeBouton.getPrefWidth());
        btnVisualiser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                i.visualiserPartie();
            }
        });

        listeDeBouton.getChildren().addAll(btnDebutIAF, btnDebutIAD, btnCharger, btnSauvegarder, btnVisualiser);

        setMenu(EtatPartie.commencer);

        return listeDeBouton;
    }

    public void setMenu(EtatPartie etat) {
        if (etat.commencer.equals(etat)) {
            btnDebutIAF.setDisable(false);
            btnDebutIAD.setDisable(false);
            btnCharger.setDisable(false);
            btnSauvegarder.setDisable(true);
            btnVisualiser.setDisable(true);
        } else if (etat.enCours.equals(etat)) {
            btnDebutIAF.setDisable(true);
            btnDebutIAD.setDisable(true);
            btnCharger.setDisable(false);
            btnSauvegarder.setDisable(false);
            btnVisualiser.setDisable(true);
        } else if (etat.finie.equals(etat)) {
            btnDebutIAF.setDisable(false);
            btnDebutIAD.setDisable(false);
            btnCharger.setDisable(false);
            btnSauvegarder.setDisable(true);
            btnVisualiser.setDisable(false);
        } else if (etat.toutCacher.equals(etat)) {
            btnDebutIAF.setDisable(true);
            btnDebutIAD.setDisable(true);
            btnCharger.setDisable(true);
            btnSauvegarder.setDisable(true);
            btnVisualiser.setDisable(true);
        }
    }

    public Parent afficherJeu() {
        HBox plateauTmp = new HBox();
        plateauTmp.getChildren().addAll(setPlateau(), setMenuPlateau());
        return plateauTmp;
    }

    public void afficherInfoPartie(int seconde, HBox container, String text) {
        container.getChildren().clear();
        dessinerText.ajouterTextForXSeconde(seconde, container, text);
    }

    public void setInfoPlateau(String nomJB, String nomJN) {
        joueurBlanc.setText(nomJB);
        nbJetonJB.setText("2 jeton(s) blanc");
        joueurNoir.setText(nomJN);
        nbJetonJN.setText("2 jeton(s) noir");

        joueurBlanc.setVisible(true);
        nbJetonJB.setVisible(true);
        joueurNoir.setVisible(true);
        nbJetonJN.setVisible(true);

        stat.setVisible(true);
    }

    public void refesh(Plateau p, Boolean doitJouer) {
        int nbB = 0;
        int nbN = 0;

        List<EtatCase> list = p.getEtatPlateau().getMatricePlateau();
        for (int x = 0; x < list.size(); x++) {
            EtatCase e = list.get(x);
            Pane pTmp = listeCase.get(x);
            pTmp.setOnMouseClicked(null);
            pTmp.getChildren().clear();

            final int pos = x;
            if (EtatCase.blanc.equals(e)) {
                dessinerJeton.dessinerUnJeton(pTmp, Color.WHITE);
                nbB++;
            } else if (EtatCase.noir.equals(e)) {
                dessinerJeton.dessinerUnJeton(pTmp, Color.BLACK);
                nbN++;
            } else if (EtatCase.jouable.equals(e) && doitJouer) {
                dessinerJeton.dessinerUnJeton(pTmp, Color.GRAY);
                pTmp.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        i.jouerUnCoup(pos);
                    }
                });
            }
        }
        nbJetonJB.setText(nbB + " jeton(s) blanc");
        nbJetonJN.setText(nbN + " jeton(s) noir");
    }

    private Button creerBouton(String text, Double w) {
        Button btn = new Button(text);
        btn.setMinWidth(w);
        btn.setMinHeight(heigth / 5);
        return btn;
    }

    private VBox setPlateau() {
        VBox plateau = new VBox();
        plateau.setPrefWidth(width - widthAffihcerInfo - widthMenu);

        double wCase = (width - widthAffihcerInfo - widthMenu) / 8;
        double hCase = (heigth) / 8;
        for (int x = 0; x < 8; x++) {
            HBox ligne = new HBox();

            for (int y = 0; y < 8; y++) {
                Pane tmp = new Pane();
                tmp.setPrefWidth(wCase);
                tmp.setPrefHeight(hCase);
                listeCase.add(tmp);
                tmp.setStyle("-fx-border-color: black");
                ligne.getChildren().add(tmp);
            }

            plateau.getChildren().add(ligne);
        }
        return plateau;
    }

    private VBox setMenuPlateau() {
        VBox infoPLateau = new VBox(20);
        infoPLateau.setPrefWidth(widthAffihcerInfo);

        infoPLateau.setStyle("-fx-padding: 5px;");

        stat = new Label("Statistique");
        stat.setFont(new Font("Arial", 22));
        stat.setVisible(false);

        VBox bTmp = new VBox(5);
        VBox bTmp2 = new VBox(5);

        joueurBlanc = new Label();
        joueurBlanc.setVisible(false);
        joueurBlanc.setFont(new Font("Arial", 16));

        nbJetonJB = new Label();
        nbJetonJB.setVisible(false);
        nbJetonJB.setFont(new Font("Arial", 12));

        joueurNoir = new Label();
        joueurNoir.setVisible(false);
        joueurNoir.setFont(new Font("Arial", 16));

        nbJetonJN = new Label();
        nbJetonJN.setVisible(false);
        nbJetonJN.setFont(new Font("Arial", 12));

        bTmp.getChildren().addAll(joueurBlanc, nbJetonJB);
        bTmp2.getChildren().addAll(joueurNoir, nbJetonJN);

        infoPLateau.getChildren().addAll(stat, bTmp, bTmp2);
        return infoPLateau;
    }

}
