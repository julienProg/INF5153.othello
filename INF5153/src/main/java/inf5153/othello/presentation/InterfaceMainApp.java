package inf5153.othello.presentation;

import inf5153.othello.service.xml.XMLSauvegardeService;
import inf5153.othello.game.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class InterfaceMainApp extends Application implements Observer {

    private PartieLocal partieL;
    private Parent plateauTmp;
    private Fenetre fenetre;
    private boolean partieEnligne;
    private CouleurJeton joueurCourant;
    private Boolean isEnLigne;
    private HBox container;
    final private XMLSauvegardeService sauvegardeService = new XMLSauvegardeService();

    //Construit l'interface
    private Parent construireInterface() {

        Pane root = new Pane();
        root.setPrefSize(1200, 800);
        root.setStyle("-fx-background-color: #F5F5DC");
        HBox lignePrincipale = new HBox();

        container = new HBox();
        container.setPrefSize(800, 200);
        container.setLayoutX((1200 / 2) - 400);
        container.setLayoutY((800 / 2) - 200);
        container.setAlignment(Pos.CENTER);

        container.setVisible(false);
        container.toBack();

        fenetre = new Fenetre((int) root.getPrefWidth(), (int) root.getPrefHeight(), this, 200, 200);
        lignePrincipale.getChildren().addAll(fenetre.afficherMenu(), fenetre.afficherJeu());
        root.getChildren().addAll(lignePrincipale, container);

        return root;
    }

    //Créer plateau (FENETRE)

    //Créer case -> cercle (noir ou blanc ou rien)

    //Créer MENU ici

    public void commencerUnePartie(TypeIA type) {
        partieEnligne = false;
        partieL = new PartieLocal();
        double tmp = Math.random();
        CouleurJeton j1;
        CouleurJeton j2;

        if (tmp < 0.5) {
            j1 = CouleurJeton.Blanc;
            j2 = CouleurJeton.Noir;
            fenetre.setInfoPlateau("Vous", "Ordinateur");
        } else {
            j1 = CouleurJeton.Noir;
            j2 = CouleurJeton.Blanc;
            fenetre.setInfoPlateau("Ordinateur", "Vous");
        }

        partieL.creerJoueurHumain(j1);
        partieL.creerJoueurIA(j2, type);
        partieL.creerPlateau(this);

        joueurCourant = j1;

        fenetre.setMenu(EtatPartie.enCours);
        verifierTour();
    }

    public void charger() {
        fenetre.setMenu(EtatPartie.enCours);
        this.partieL = sauvegardeService.chargerLaPartie("test.xml");
        partieEnligne = false;

        if (this.partieL.getCouleurCourant() == this.partieL.getCouleurOrdi()) {

            fenetre.setInfoPlateau("Ordinateur", "Vous");
        } else {
            fenetre.setInfoPlateau("Vous", "Ordinateur");
        }
//
//        partieL.creerJoueurHumain(this.partieL.creerJoueurIA(couleur, type););
//        partieL.creerJoueurIA(j2, type);
//        partieL.creerPlateau(this);
        partieL.getPlateauCourant().ajouterObservable(this);

        joueurCourant = this.partieL.getCouleurCourant();

        fenetre.setMenu(EtatPartie.enCours);
        verifierTour();
    }

    public void sauvegarder() {
        fenetre.setMenu(EtatPartie.enCours);
        PartieLocal partieLocal = this.partieL;
        if (partieLocal != null) {
            sauvegardeService.sauvegarderLaPartie(partieLocal);
        } else {
            throw new IllegalStateException("Partie est null");
        }
    }

    public void visualiserPartie() {
        fenetre.setMenu(EtatPartie.toutCacher);
        voirCoupAfterXSeconde(1);
    }

    private void voirCoupAfterXSeconde(int seconde) {
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(seconde),
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (!partieL.revoirPartie()) {
                            voirCoupAfterXSeconde(1);
                        } else {
                            fenetre.setMenu(EtatPartie.finie);
                        }
                    }
                }));
        timeline.play();
    }

    private void verifierTour() {
        int i = partieL.verifierJouabilite();
        int seconde = 3;

        switch (i) {
//            case 1 :
//                if(!joueurCourant.equals(couleurCourant)){
//                    //verifierTour(); //ordi doit jouer
//                }
//                break;
            case 2:
                CouleurJeton couleurCourant = partieL.getCouleurCourant();

                fenetre.afficherInfoPartie(seconde, container, "Le joueur " + couleurCourant.toString() + " passe son tour");
                partieL.tourSuivant();


                Timeline timeline = new Timeline();
                timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(seconde),
                        new EventHandler<ActionEvent>() {

                            @Override
                            public void handle(ActionEvent event) {
                                verifierTour();
                            }
                        }));
                timeline.play();

                break;
            case 3:
                fenetre.afficherInfoPartie(3, container, "La Partie est terminée!");
                fenetre.setMenu(EtatPartie.finie);
                break;
        }

    }

    public void update(Plateau p) {
        fenetre.refesh(p, partieL.getCouleurCourant().equals(joueurCourant));
    }

    public void jouerUnCoup(int pos) {
        if (!partieEnligne) {
            jouerUnCoupLocal(pos);
            return;
        }
        jouerUnCoupEnLigne(pos);
    }

    private void jouerUnCoupLocal(int pos) {
        partieL.jouerUnJeton(pos);
        verifierTour();
    }

    private void jouerUnCoupEnLigne(int pos) {
        //Pas implémenter
    }


    @Override
    public void start(Stage stage) throws Exception {
        Parent root = construireInterface();
        Scene scene = new Scene(root);
        stage.setTitle("Othello");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
