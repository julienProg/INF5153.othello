/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.service.xml;

import javax.xml.bind.*;

import inf5153.othello.game.Partie;
import inf5153.othello.game.PartieLocal;

import java.io.File;

//http://stackoverflow.com/questions/7373567/java-how-to-read-and-write-xml-files

/**
 *
 * @author Julien
 */
public class XMLSauvegardeService {

    final JAXBContext context ;
    final Unmarshaller unmarshaller;
    final Marshaller marshaller;

    public XMLSauvegardeService(){


        JAXBContext _context = null;
        Unmarshaller _unmarshaller = null;
        Marshaller _marshaller = null;

        try {
            _context = JAXBContext.newInstance(PartieLocal.class);
            _unmarshaller = _context.createUnmarshaller();
            _marshaller = _context.createMarshaller();
            _marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (JAXBException e) {
            throw new IllegalStateException(e);
        }
        context = _context;
        unmarshaller = _unmarshaller;
        marshaller = _marshaller;
    }

    public boolean sauvegarderLaPartie(PartieLocal partie){

        try {
            marshaller.marshal(partie, new File("test.xml"));
        } catch (JAXBException e) {
            throw new IllegalStateException(e);
        }


        return true;
    }

    public PartieLocal chargerLaPartie(String nomDePartie){
        try {
            return  (PartieLocal) unmarshaller.unmarshal(new File(nomDePartie)) ;
        } catch (JAXBException e) {
            throw new IllegalStateException(e);
        }
    }
}
