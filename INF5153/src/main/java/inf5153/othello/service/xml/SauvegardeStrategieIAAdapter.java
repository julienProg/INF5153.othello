package inf5153.othello.service.xml;


import inf5153.othello.game.IAGrosCoup;
import inf5153.othello.game.IAHasard;
import inf5153.othello.game.StrategieIA;
import inf5153.othello.game.TypeIA;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by cabosmarc on 2016-07-23.
 */
public class SauvegardeStrategieIAAdapter extends XmlAdapter<TypeIA, StrategieIA> {
    @Override
    public StrategieIA unmarshal(TypeIA type) throws Exception {

        if (TypeIA.grosCoup.equals(type)) {
            return new IAGrosCoup();
        } else {
            return new IAHasard();
        }
    }

    @Override
    public TypeIA marshal(StrategieIA strategie) throws Exception {
        if (strategie instanceof IAGrosCoup) {
            return TypeIA.grosCoup;
        } else {
            return TypeIA.hasard;
        }
    }
}
