/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.game;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 * @author Julien
 */

public class IAGrosCoup implements StrategieIA {
    
    public IAGrosCoup()
    {
    }
    
    public Integer calculPositionnement(Plateau p, CouleurJeton col){
        int position = 0;
        int max = 0;
        int tmpval;
        List<Integer> plat = p.getListeJouable();
        
        for (int each : plat)
        {
            tmpval = p.valeurCoup(each, col).size();
            if (tmpval>max)
            {
                max = tmpval;
                position = each;
            }
        }
     return position;   
    }
}
