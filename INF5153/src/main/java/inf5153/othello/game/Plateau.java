/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.game;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Julien
 */

@XmlRootElement
public class Plateau {

    final private List<Observer> listObservable= new ArrayList<>();
    @XmlElement
    final private EtatPlateau etatPlateau = new EtatPlateau();

    private List<Integer> listJouable;

    public EtatPlateau getEtatPlateau(){
        return etatPlateau;
    }
    
    /**
     * Calcul les coups possibles et change l'etat des cases concernees
     * @param couleur couleur du joueur dont c<est le tour
     * @return 
     */
    public boolean calculCoupPossible (CouleurJeton couleur){
        listJouable = new ArrayList<>();
        
        boolean estJouable = false;
        
        EtatCase couleurV;
        if (couleur.equals(CouleurJeton.Blanc)){
            couleurV = EtatCase.blanc;
        }else{
            couleurV = EtatCase.noir;
        }   
            
        for (int i=0; i<64; i++)
        {
            if (etatPlateau.getMatricePlateau().get(i).equals(EtatCase.jouable))
                    etatPlateau.getMatricePlateau().set(i, EtatCase.rien);
            
            if (etatPlateau.getMatricePlateau().get(i).equals(EtatCase.rien)){
                if (etatPlateau.calcJouable(i, couleurV)){
                    etatPlateau.getMatricePlateau().set(i, EtatCase.jouable);
                    listJouable.add(i);
                    estJouable = true;
                }
            }
        }
        
        return estJouable;
    }
    
    public void mettreAJourPlateau(int pos, CouleurJeton couleur){
        EtatCase e;
        if(CouleurJeton.Blanc.equals(couleur)){
            e = EtatCase.blanc;
        }else{
            e = EtatCase.noir;
        }
        
        etatPlateau.getMatricePlateau().set(pos,e);
        List<Integer> liste = etatPlateau.valeurCoup(pos,e);
        
        for (int each : liste)
            etatPlateau.getMatricePlateau().set(each, e);
    }
    
    public List<Integer> getListeJouable(){
        return listJouable;
    }
    
    public void ajouterObservable (Observer o){
        listObservable.add(o);
    }
    
    public void supprimerObservable (Observer o){
        listObservable.remove(o);
    }
    
    public void mettreAJour (){
        for (Observer each : listObservable) {
            each.update(this);
        }
    }
    
    public void setEtatPlateau(List<EtatCase> plat)
    {
        etatPlateau.setMatricePlateau(plat);
        mettreAJour();
    }
    
    /**
     * Retourne la liste des cases affectees par un coup (pour le changement de couleur)
     * @param pos la position du jeton joue
     * @param couleur la couleur du jeton joue
     * @return la liste des case affectees
     */
    public List<Integer> valeurCoup(int pos, CouleurJeton couleur)
    {        
        if (couleur.equals(CouleurJeton.Blanc)){
            return etatPlateau.valeurCoup(pos, EtatCase.blanc);
        }else{
            return etatPlateau.valeurCoup(pos, EtatCase.noir);
        }
    }
    

    

}
