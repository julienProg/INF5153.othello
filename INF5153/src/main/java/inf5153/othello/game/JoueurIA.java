/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.game;

import inf5153.othello.service.xml.SauvegardeStrategieIAAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;
/**
 *
 * @author Julien
 */
public class JoueurIA extends Joueur{

    @XmlJavaTypeAdapter(SauvegardeStrategieIAAdapter.class)
    private StrategieIA iaType;

    public JoueurIA(){}

    public JoueurIA (TypeIA type){
       StrategieIA ia;
       if(TypeIA.grosCoup.equals(type)){
           ia = new IAGrosCoup();
       }else{
           ia = new IAHasard();         
       }
       iaType = ia;
    }
    
    public int jouerUnJeton(Plateau p){
        List<EtatCase> list = p.getEtatPlateau().getMatricePlateau();
        return iaType.calculPositionnement(p, getCouleur());  
    }
    
}
