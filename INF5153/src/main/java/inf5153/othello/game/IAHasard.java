/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.game;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Random;
/**
 *
 * @author Julien
 */

public class IAHasard implements StrategieIA {
    
    public Integer calculPositionnement(Plateau p, CouleurJeton col){
        List<Integer> plat = p.getListeJouable();
               
        Random r = new Random();
        int valeur = r.nextInt(plat.size());

        return plat.get(valeur);
    }
}
