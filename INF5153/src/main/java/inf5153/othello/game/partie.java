/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.game;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Julien
 */

@XmlRootElement
public abstract class Partie {

    @XmlElement
    final private List<EtatPlateau> listeDeCoup;
    @XmlElement
    private CouleurJeton couleurCourant;
    @XmlElement
    private Joueur joueurInterface;
    @XmlElement
    private boolean passerTour;
    @XmlElement
    private Plateau plateauDeJeux;
    @XmlElement
    private CouleurJeton couleurJoueurInterface;



    @XmlElement
    private int positionList;

    public Partie() {
        couleurCourant = CouleurJeton.Noir;
        passerTour = false;
        listeDeCoup = new ArrayList<>();
        positionList = 0;
    }

    protected Partie(Partie partie) {
        this.listeDeCoup = new ArrayList<>(partie.listeDeCoup);
        this.couleurCourant = partie.couleurCourant;
        this.joueurInterface = partie.joueurInterface;
        this.passerTour = partie.passerTour;
        this.plateauDeJeux = partie.plateauDeJeux;
        this.couleurJoueurInterface = partie.couleurJoueurInterface;
        this.positionList = partie.positionList;
    }

    public void creerJoueurHumain(CouleurJeton couleur) {
        joueurInterface = new JoueurHumain("127.0.0.1");
        joueurInterface.setCouleur(couleur);
        couleurJoueurInterface = couleur;
    }

    public void creerPlateau(Observer interfacePlateau) {
        plateauDeJeux = new Plateau();
        plateauDeJeux.ajouterObservable(interfacePlateau);
        EtatPlateau tmp = new EtatPlateau(plateauDeJeux.getEtatPlateau().getMatricePlateau());
        listeDeCoup.add(tmp);
    }

    public int verifierJouabilite() {

        if (!plateauDeJeux.calculCoupPossible(couleurCourant)) {

            //Partie terminé
            if (passerTour) {
                return 3;
            }

            //Passer son tour
            passerTour = true;
            return 2; //Le joueur a passer sont tour
        }
        passerTour = false;
        return 1;
    }

    public void tourSuivant() {
        if (CouleurJeton.Blanc.equals(couleurCourant)) {
            couleurCourant = CouleurJeton.Noir;
            return;
        }
        couleurCourant = CouleurJeton.Blanc;
    }

    public CouleurJeton getCouleurCourant() {
        return couleurCourant;
    }

    public Plateau getPlateauCourant() {
        return plateauDeJeux;
    }

    public void jouerUnJeton(int pos) {

        plateauDeJeux.mettreAJourPlateau(pos, couleurCourant);
        listeDeCoup.add(new EtatPlateau(
                this.plateauDeJeux.getEtatPlateau().getMatricePlateau()));
        tourSuivant();
    }

    public boolean revoirPartie()
    {
        boolean estFini = false;
        plateauDeJeux.setEtatPlateau(listeDeCoup.get(positionList).getMatricePlateau());
        positionList++;
        if(positionList >= listeDeCoup.size()){
            estFini = true;
            positionList = 0;
        }
        return estFini;
    }

}