/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.game;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Julien
 */

public class JoueurHumain extends Joueur {


    private String localisation;


    public JoueurHumain(String localisation){
        this.localisation = localisation;
    }

    public String getLocalisation() {
        return localisation;
    }
}
