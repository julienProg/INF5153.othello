/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.game;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Julien
 */
@XmlRootElement
public class PartieLocal extends Partie {

    @XmlElement
    private JoueurIA ordi;
    @XmlElement
    private CouleurJeton couleurOrdi;

    public CouleurJeton getCouleurOrdi() {
        return couleurOrdi;
    }

    public PartieLocal() {
        super();
    }

    public PartieLocal(Partie partie) {
        super(partie);
    }

    public void creerJoueurIA(CouleurJeton couleur, TypeIA type) {
        ordi = new JoueurIA(type);
        ordi.setCouleur(couleur);
        couleurOrdi = couleur;
    }

    public int verifierJouabilite() {

        //Verifier si on peut jouer
        int tmp = super.verifierJouabilite();

        //Met a jour visuelle
        Plateau p = super.getPlateauCourant();
        p.mettreAJour();

        if (tmp == 1) {
            if (couleurOrdi.equals(super.getCouleurCourant())) {
                int i = ordi.jouerUnJeton(p);
                super.jouerUnJeton(i);

                return verifierJouabilite();
            }
        }
        return tmp;
    }
}
