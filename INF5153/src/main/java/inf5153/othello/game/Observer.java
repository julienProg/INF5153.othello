/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.game;

/**
 *
 * @author Julien
 */
public interface Observer {
    public void update(Plateau subject);
}
