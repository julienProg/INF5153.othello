package inf5153.othello.game;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cabosmarc on 2016-07-23.
 */
@XmlRootElement
public class EtatPlateau {

    private List<EtatCase> matricePlateau;

    public EtatPlateau(){
        this(null);
    }

    public EtatPlateau(List<EtatCase> matricePlateau){
        if (matricePlateau == null){
            this.matricePlateau = new ArrayList<>();
        }else {
            this.matricePlateau = new ArrayList<>(matricePlateau);
            return;
        }

        for (int i=0; i<64; i++){
            this.matricePlateau.add(EtatCase.rien);
        }
        this.matricePlateau.set(27, EtatCase.blanc);
        this.matricePlateau.set(28, EtatCase.noir);
        this.matricePlateau.set(35, EtatCase.noir);
        this.matricePlateau.set(36, EtatCase.blanc);
    }

    public void setMatricePlateau(List<EtatCase> plat) {
        this.matricePlateau = plat;
    }

    public List<EtatCase> getMatricePlateau() {
        return this.matricePlateau;
    }
    /**
     * Retourne la liste des cases affectees par un coup (pour le changement de couleur)
     * @param pos la position du jeton joue
     * @param couleur la couleur du jeton joue
     * @return la liste des case affectees
     */
    public List<Integer> valeurCoup(int pos, EtatCase couleur)
    {
        List<Integer> liste = new ArrayList<>();
        switch (calcPosition(pos)){
            case 0:
                liste.addAll(dC(pos, couleur));
                liste.addAll(bdC(pos, couleur));
                liste.addAll(bC(pos, couleur));
                return liste;
            case 1:
                liste.addAll(bC(pos, couleur));
                liste.addAll(bgC(pos, couleur));
                liste.addAll(gC(pos, couleur));
                return liste;
            case 2:
                liste.addAll(hC(pos, couleur));
                liste.addAll(hdC(pos, couleur));
                liste.addAll(dC(pos, couleur));
                return liste;
            case 3:
                liste.addAll(gC(pos, couleur));
                liste.addAll(hgC(pos, couleur));
                liste.addAll(hC(pos, couleur));
                return liste;
            case 4:
                liste.addAll(gC(pos, couleur));
                liste.addAll(hgC(pos, couleur));
                liste.addAll(hC(pos, couleur));
                liste.addAll(hdC(pos, couleur));
                liste.addAll(dC(pos, couleur));
                return liste;
            case 5:
                liste.addAll(dC(pos, couleur));
                liste.addAll(bdC(pos, couleur));
                liste.addAll(bC(pos, couleur));
                liste.addAll(bgC(pos, couleur));
                liste.addAll(gC(pos, couleur));
                return liste;
            case 6:
                liste.addAll(hC(pos, couleur));
                liste.addAll(hdC(pos, couleur));
                liste.addAll(dC(pos, couleur));
                liste.addAll(bdC(pos, couleur));
                liste.addAll(bC(pos, couleur));
                return liste;
            case 7:
                liste.addAll(bC(pos, couleur));
                liste.addAll(bgC(pos, couleur));
                liste.addAll(gC(pos, couleur));
                liste.addAll(hgC(pos, couleur));
                liste.addAll(hC(pos, couleur));
                return liste;
            case 8:
                liste.addAll(hC(pos, couleur));
                liste.addAll(hdC(pos, couleur));
                liste.addAll(dC(pos, couleur));
                liste.addAll(bdC(pos, couleur));
                liste.addAll(bC(pos, couleur));
                liste.addAll(bgC(pos, couleur));
                liste.addAll(gC(pos, couleur));
                liste.addAll(hgC(pos, couleur));
                return liste;
        }
        return liste;
    }

    /**
     * Remplace les cases jouables par des cases vides.
     */
    private void cleanPlateau(){
        for (int i=0;i<64;i++)
            if (matricePlateau.get(i).equals(EtatCase.jouable))
                matricePlateau.set(i, EtatCase.rien);
    }


    /**
     * Calcul si une case est jouable
     * @param pos position de la case a verifier
     * @param couleur couleur du joueur courant
     * @return position jouable ou pas
     */
    public boolean calcJouable(int pos, EtatCase couleur){
        switch (calcPosition(pos)){
            case 0:
                return (dJ(pos, couleur)||bdJ(pos, couleur)||bJ(pos,couleur));
            case 1:
                return (bJ(pos, couleur)||bgJ(pos,couleur)||gJ(pos,couleur));
            case 2:
                return (hJ(pos, couleur)||hdJ(pos, couleur)||dJ(pos, couleur));
            case 3:
                return (gJ(pos, couleur)||hgJ(pos, couleur)||hJ(pos, couleur));
            case 4:
                return (gJ(pos, couleur)||hgJ(pos, couleur)||hJ(pos, couleur)||hdJ(pos, couleur)||dJ(pos, couleur));
            case 5:
                return (dJ(pos, couleur)||bdJ(pos, couleur)||bJ(pos, couleur)||bgJ(pos, couleur)||gJ(pos, couleur));
            case 6:
                return (hJ(pos, couleur)||hdJ(pos, couleur)||dJ(pos, couleur)||bdJ(pos, couleur)||bJ(pos, couleur));
            case 7:
                return (bJ(pos, couleur)||bgJ(pos, couleur)||gJ(pos, couleur)||hgJ(pos, couleur)||hJ(pos, couleur));
            case 8:
                return (hJ(pos, couleur)||hdJ(pos, couleur)||dJ(pos, couleur)||bdJ(pos, couleur)||bJ(pos, couleur)||bgJ(pos, couleur)||gJ(pos, couleur)||hgJ(pos, couleur));
        }
        return false;
    }

    /**
     * Determine a quel endroit sur le matricePlateau se trouve une position
     * @param pos la position a verifier
     * @return int de 0-7 indiquant l'endroit
     *
     */
    private int calcPosition(int pos)
    {
        if (pos == 0)                       //coin haut gauche
            return 0;
        else if (pos == 7)                  //coin haut droit
            return 1;
        else if (pos == 56)                 //coin bas gauche
            return 2;
        else if (pos == 63)                 //coin bas droit
            return 3;
        else if (pos >= 56)                 //ligne bas
            return 4;
        else if (pos <= 7)                  //ligne haut
            return 5;
        else if (pos % 8 == 0)              //ligne gauche
            return 6;
        else if (pos % 8 == 7)              //ligne droite
            return 7;
        else                                //milieu
            return 8;
    }

    /**
     *
     * @param couleur la couleur a inverser
     * @return la couleur inverse
     */
    private EtatCase couleurInverse(EtatCase couleur)
    {
        if (couleur.equals(EtatCase.noir))
            return EtatCase.blanc;
        else
            return EtatCase.noir;
    }
    
    /*Les fonctions qui suivent servent à vérifier la jouabilité dans une direction
    h:haut
    b:bas
    d:droite
    g:gauche*/

    private boolean hJ(int pos, EtatCase couleur)
    {
        if (!matricePlateau.get(pos-8).equals(couleurInverse(couleur))||(pos<16))
            return false;
        int i = pos-16;
        do
        {
            if (matricePlateau.get(i).equals(couleur))
                return true;
            else if (matricePlateau.get(i).equals(EtatCase.rien)|| matricePlateau.get(i).equals(EtatCase.jouable))
                return false;
            i-=8;
        }while (i>=0);
        return false;
    }

    private boolean hdJ(int pos, EtatCase couleur)
    {
        if (!matricePlateau.get(pos-7).equals(couleurInverse(couleur))||(pos%8==6)||(pos<16))
            return false;
        int i = pos-14;
        do
        {
            if (matricePlateau.get(i).equals(couleur))
                return true;
            else if (matricePlateau.get(i).equals(EtatCase.rien)|| matricePlateau.get(i).equals(EtatCase.jouable))
                return false;
            i-=7;
        }while ((i%8!=0)&&(i>=0));
        return false;
    }

    private boolean dJ(int pos, EtatCase couleur)
    {
        if (!matricePlateau.get(pos+1).equals(couleurInverse(couleur))||(pos%8==6))
            return false;
        int i = pos+2;
        do
        {
            if (matricePlateau.get(i).equals(couleur))
                return true;
            else if (matricePlateau.get(i).equals(EtatCase.rien)|| matricePlateau.get(i).equals(EtatCase.jouable))
                return false;
            i++;
        }while (i%8!=0);
        return false;
    }

    private boolean bdJ(int pos, EtatCase couleur)
    {
        if (!matricePlateau.get(pos+9).equals(couleurInverse(couleur))||(pos%8==6)||(pos>47))
            return false;
        int i = pos+18;
        do
        {
            if (matricePlateau.get(i).equals(couleur))
                return true;
            else if (matricePlateau.get(i).equals(EtatCase.rien)|| matricePlateau.get(i).equals(EtatCase.jouable))
                return false;
            i+=9;
        }while ((i%8!=0)&&(i<=63));
        return false;
    }

    private boolean bJ(int pos, EtatCase couleur)
    {
        if (!matricePlateau.get(pos+8).equals(couleurInverse(couleur))||(pos>47))
            return false;
        int i = pos+16;
        do
        {
            if (matricePlateau.get(i).equals(couleur))
                return true;
            else if (matricePlateau.get(i).equals(EtatCase.rien)|| matricePlateau.get(i).equals(EtatCase.jouable))
                return false;
            i+=8;
        }while (i<=63);
        return false;
    }

    private boolean bgJ(int pos, EtatCase couleur)
    {
        if (!matricePlateau.get(pos+7).equals(couleurInverse(couleur))||(pos%8==1)||(pos>47))
            return false;
        int i = pos+14;
        do
        {
            if (matricePlateau.get(i).equals(couleur))
                return true;
            else if (matricePlateau.get(i).equals(EtatCase.rien)|| matricePlateau.get(i).equals(EtatCase.jouable))
                return false;
            i+=7;
        }while ((i%8!=7)&&(i<=63));
        return false;
    }

    private boolean gJ(int pos, EtatCase couleur)
    {
        if (!matricePlateau.get(pos-1).equals(couleurInverse(couleur))||(pos%8==1))
            return false;
        int i = pos-2;
        do
        {
            if (matricePlateau.get(i).equals(couleur))
                return true;
            else if (matricePlateau.get(i).equals(EtatCase.rien)|| matricePlateau.get(i).equals(EtatCase.jouable))
                return false;
            i--;
        }while (i%8!=7&&i>=0);                      //en java, modulo sur un chiffre negatif retourne une val negative...
        return false;
    }

    private boolean hgJ(int pos, EtatCase couleur)
    {
        if (!matricePlateau.get(pos-9).equals(couleurInverse(couleur))||(pos%8==1)||(pos<16))
            return false;
        int i = pos-18;
        do
        {
            if (matricePlateau.get(i).equals(couleur))
                return true;
            else if (matricePlateau.get(i).equals(EtatCase.rien)|| matricePlateau.get(i).equals(EtatCase.jouable))
                return false;
            i-=9;
        }while ((i%8!=7)&&(i>=0));
        return false;
    }
    
    /*Les fonctions qui suivent retournent une liste de case qui doivent changer de couleur
    *
    *
    */

    private List<Integer> hC(int pos, EtatCase couleur)
    {
        List<Integer> liste = new ArrayList<>();
        EtatCase cInv = couleurInverse(couleur);

        if (!matricePlateau.get(pos-8).equals(cInv)||(pos<16))
            return liste;

        liste.add(pos-8);
        int i = pos-16;
        do
        {
            if (matricePlateau.get(i).equals(couleurInverse(couleur)))
                liste.add(i);
            else if (matricePlateau.get(i).equals(couleur))
                return liste;
            else
                return new ArrayList<>();
            i-=8;
        }while (i>=0);
        return new ArrayList<>();
    }

    private List<Integer> hdC(int pos, EtatCase couleur)
    {
        List<Integer> liste = new ArrayList<>();
        EtatCase cInv = couleurInverse(couleur);

        if (!matricePlateau.get(pos-7).equals(cInv)||(pos%8==6)||(pos<16))
            return liste;

        liste.add(pos-7);
        int i = pos-14;
        do
        {
            if (matricePlateau.get(i).equals(cInv))
                liste.add(i);
            else if (matricePlateau.get(i).equals(couleur))
                return liste;
            else
                return new ArrayList<>();
            i-=7;
        }while ((i%8!=0)&&(i>=0));
        return new ArrayList<>();
    }

    private List<Integer> dC(int pos, EtatCase couleur)
    {
        List<Integer> liste = new ArrayList<>();
        EtatCase cInv = couleurInverse(couleur);

        if (!matricePlateau.get(pos+1).equals(cInv)||(pos%8==6))
            return liste;

        liste.add(pos+1);
        int i = pos+2;
        do
        {
            if (matricePlateau.get(i).equals(cInv))
                liste.add(i);
            else if (matricePlateau.get(i).equals(couleur))
                return liste;
            else
                return new ArrayList<>();
            i++;
        }while (i%8!=0);
        return new ArrayList<>();
    }

    private List<Integer> bdC(int pos, EtatCase couleur)
    {
        List<Integer> liste = new ArrayList<>();
        EtatCase cInv = couleurInverse(couleur);

        if (!matricePlateau.get(pos+9).equals(cInv)||(pos%8==6)||(pos>47))
            return liste;

        liste.add(pos+9);
        int i = pos+18;
        do
        {
            if (matricePlateau.get(i).equals(cInv))
                liste.add(i);
            else if (matricePlateau.get(i).equals(couleur))
                return liste;
            else
                return new ArrayList<>();
            i+=9;
        }while ((i%8!=0)&&(i<=63));
        return new ArrayList<>();
    }

    private List<Integer> bC(int pos, EtatCase couleur)
    {
        List<Integer> liste = new ArrayList<>();
        EtatCase cInv = couleurInverse(couleur);

        if (!matricePlateau.get(pos+8).equals(cInv)||(pos>47))
            return liste;

        liste.add(pos+8);
        int i = pos+16;
        do
        {
            if (matricePlateau.get(i).equals(cInv))
                liste.add(i);
            else if (matricePlateau.get(i).equals(couleur))
                return liste;
            else
                return new ArrayList<>();
            i+=8;
        }while (i<=63);
        return new ArrayList<>();
    }

    private List<Integer> bgC(int pos, EtatCase couleur)
    {
        List<Integer> liste = new ArrayList<>();
        EtatCase cInv = couleurInverse(couleur);

        if (!matricePlateau.get(pos+7).equals(cInv)||(pos%8==1)||(pos>47))
            return liste;

        liste.add(pos+7);
        int i = pos+14;
        do
        {
            if (matricePlateau.get(i).equals(cInv))
                liste.add(i);
            else if (matricePlateau.get(i).equals(couleur))
                return liste;
            else
                return new ArrayList<>();
            i+=7;
        }while ((i%8!=7)&&(i<=63));
        return new ArrayList<>();
    }

    private List<Integer> gC(int pos, EtatCase couleur)
    {
        List<Integer> liste = new ArrayList<>();
        EtatCase cInv = couleurInverse(couleur);

        if (!matricePlateau.get(pos-1).equals(cInv)||(pos%8==1))
            return liste;

        liste.add(pos-1);
        int i = pos-2;
        do
        {
            if (matricePlateau.get(i).equals(cInv))
                liste.add(i);
            else if (matricePlateau.get(i).equals(couleur))
                return liste;
            else
                return new ArrayList<>();
            i--;
        }while (i%8!=7&&i>=0);                      //en java, modulo sur un chiffre negatif retourne une val negative...
        return new ArrayList<>();
    }
    private List<Integer> hgC(int pos, EtatCase couleur)
    {
        List<Integer> liste = new ArrayList<>();
        EtatCase cInv = couleurInverse(couleur);

        if (!matricePlateau.get(pos-9).equals(cInv)||(pos%8==1)||(pos<16))
            return liste;

        liste.add(pos-9);
        int i = pos-18;
        do
        {
            if (matricePlateau.get(i).equals(cInv))
                liste.add(i);
            else if (matricePlateau.get(i).equals(couleur))
                return liste;
            else
                return new ArrayList<>();
            i-=9;
        }while ((i%8!=7)&&(i>=0));
        return new ArrayList<>();
    }
}
