/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inf5153.othello.game;


import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Julien
 */
public class Joueur {

    protected CouleurJeton couleur;
    
    public Joueur(){
        
    }
    
    public void setCouleur(CouleurJeton couleur){
        this.couleur = couleur;
    }
    
    public CouleurJeton getCouleur()
    {
        return couleur;
    }
}


